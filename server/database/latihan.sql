-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 03 Okt 2019 pada 02.49
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latihan`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `col_siswa` (`p_kelas` INT) RETURNS INT(11) BEGIN
DECLARE jml INT;
SELECT COUNT(*) AS jml_kelas INTO jml FROM data_siswa WHERE kelas = p_kelas;
RETURN jml;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `segitiga` (`a` INT, `t` INT) RETURNS INT(11) BEGIN 
DECLARE result INT;
SELECT (a * t / 2) INTO result;
RETURN result;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `v_segitiga` (`t_alas` INT, `l_alas` INT, `t` INT) RETURNS INT(11) BEGIN 
DECLARE result INT;
SET result = (t_alas * l_alas /2) * t;
RETURN result;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_siswa`
--

CREATE TABLE `data_siswa` (
  `nis` char(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kelas` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_siswa`
--

INSERT INTO `data_siswa` (`nis`, `nama`, `kelas`) VALUES
('1211321231', 'jia lissa', 11),
('1231231231', 'Airlengga', 12),
('9909290191', 'Yago', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `id_jk` int(1) NOT NULL,
  `jk` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id_jk`, `jk`) VALUES
(1, 'Laki - laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(15) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `id_jk` int(1) DEFAULT NULL,
  `kelas` varchar(12) DEFAULT NULL,
  `alamat` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `id_jk`, `kelas`, `alamat`) VALUES
(1, '1', 1, '1', '1'),
(2, '1', 1, '1', '1'),
(69, 'post', 1, 'post', 'post'),
(123123, 'Ahmad Nurdin', 1, '11 RPL 1', 'Jl. Jalan Kota Bandung'),
(123124, 'Riana', 2, '11 RPL 1', 'Jl. Jalan Kota Bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_hotel`
--

CREATE TABLE `transaksi_hotel` (
  `id` int(3) NOT NULL,
  `nama_penyewa` varchar(20) NOT NULL,
  `kode_kamar` varchar(4) NOT NULL,
  `jenis_kamar` varchar(10) NOT NULL,
  `tipe_kamar` varchar(10) NOT NULL,
  `fasilitas_tambahan` varchar(25) NOT NULL,
  `biaya_sewa` int(9) NOT NULL,
  `biaya_fasilitas` int(9) NOT NULL,
  `total_sewa` int(9) NOT NULL,
  `bayar` int(9) NOT NULL,
  `kembali` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_hotel`
--

INSERT INTO `transaksi_hotel` (`id`, `nama_penyewa`, `kode_kamar`, `jenis_kamar`, `tipe_kamar`, `fasilitas_tambahan`, `biaya_sewa`, `biaya_fasilitas`, `total_sewa`, `bayar`, `kembali`) VALUES
(1, 'Hanan', 'D001', 'Double Bed', 'Superior', 'TV Kabel', 1200000, 100000, 1300000, 1999999, 699999);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_kereta`
--

CREATE TABLE `transaksi_kereta` (
  `id` int(3) NOT NULL,
  `jenis_kereta` varchar(5) NOT NULL,
  `jumlah_tiket` int(2) NOT NULL,
  `nama_kereta` varchar(15) NOT NULL,
  `harga_tiket` int(9) NOT NULL,
  `jenis_penumpang` varchar(6) NOT NULL,
  `biaya_asuransi` int(8) NOT NULL,
  `biaya` int(9) NOT NULL,
  `potongan` int(9) NOT NULL,
  `ppn` int(8) NOT NULL,
  `total_bayar` int(9) NOT NULL,
  `jumlah_uang` int(9) NOT NULL,
  `kembali` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_kereta`
--

INSERT INTO `transaksi_kereta` (`id`, `jenis_kereta`, `jumlah_tiket`, `nama_kereta`, `harga_tiket`, `jenis_penumpang`, `biaya_asuransi`, `biaya`, `potongan`, `ppn`, `total_bayar`, `jumlah_uang`, `kembali`) VALUES
(1, 'AG001', 4, 'Agro Bromo', 1000000, 'Anak', 150000, 4000000, 600000, 400000, 3000000, 8000000, 5000000),
(3, 'BS001', 4, 'Bima Sakti', 1200000, 'Anak', 180000, 4800000, 720000, 480000, 3600000, 8000000, 4400000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_kursus`
--

CREATE TABLE `transaksi_kursus` (
  `id` int(2) NOT NULL,
  `no_pendaftaran` varchar(15) NOT NULL,
  `jenis_kursus` varchar(4) NOT NULL,
  `nama_kursus` varchar(7) NOT NULL,
  `waktu_kursus` varchar(5) NOT NULL,
  `berapa_hari` int(2) NOT NULL,
  `fasilitas` varchar(11) NOT NULL,
  `biaya_kursus` int(7) NOT NULL,
  `biaya_fasilitas` int(7) NOT NULL,
  `total_pembayaran` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_kursus`
--

INSERT INTO `transaksi_kursus` (`id`, `no_pendaftaran`, `jenis_kursus`, `nama_kursus`, `waktu_kursus`, `berapa_hari`, `fasilitas`, `biaya_kursus`, `biaya_fasilitas`, `total_pembayaran`) VALUES
(5, 'cdav', 'U001', 'Umum', 'Malam', 2, 'Internet', 200000, 20000, 220000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indeks untuk tabel `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`id_jk`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `id_jk` (`id_jk`);

--
-- Indeks untuk tabel `transaksi_hotel`
--
ALTER TABLE `transaksi_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_kereta`
--
ALTER TABLE `transaksi_kereta`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_kursus`
--
ALTER TABLE `transaksi_kursus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `id_jk` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transaksi_hotel`
--
ALTER TABLE `transaksi_hotel`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaksi_kereta`
--
ALTER TABLE `transaksi_kereta`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transaksi_kursus`
--
ALTER TABLE `transaksi_kursus`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
