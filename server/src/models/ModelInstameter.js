'use strict';
module.exports = (sequelize, DataTypes) => {
    const instameter = sequelize.define('instameter', {
        username: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.STRING,
        },
        password: DataTypes.TEXT,
        profilePic: DataTypes.STRING,
        status: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        posts: DataTypes.INTEGER,
        followers: DataTypes.INTEGER,
        following: DataTypes.INTEGER,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'instameter'
    });
    instameter.associate = function(models) {
        // associations can be defined here
    };
    return instameter;
};