"use strict";
module.exports = (sequelize, DataTypes) => {
  const jenis_kelamin = sequelize.define(
    "jenis_kelamin",
    {
      id_jk: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      jk: DataTypes.STRING
    },
    {
      timestamps: false,
      freezeTableName: true,
      tableName: "jenis_kelamin"
    }
  );
  jenis_kelamin.associate = function(models) {
    // associations can be defined here
  };
  return jenis_kelamin;
};
