'use strict';
module.exports = (sequelize, DataTypes) => {
    const admin = sequelize.define('admin', {
        username: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.STRING,
        },
        password: DataTypes.TEXT,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'admin'
    });
    admin.associate = function(models) {
        // associations can be defined here
    };
    return admin;
};