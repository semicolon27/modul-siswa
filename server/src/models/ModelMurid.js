'use strict';
module.exports = (sequelize, DataTypes) => {
    const siswa = sequelize.define('siswa', {
        nis: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nama: DataTypes.STRING,
        id_jk: DataTypes.INTEGER,
        kelas: DataTypes.STRING,
        alamat: DataTypes.TEXT,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'siswa'
    });
    siswa.associate = function(models) {
        // associations can be defined here
    };
    return siswa;
};