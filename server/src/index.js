import Express from "express";
import session from 'express-session';
import httpError from "http-errors";
import Cors from "cors";
import path from "path";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import Route from "./routes/Router";
import morgan from "morgan";

const PORT = 7007;
const app = Express();
app.use(Cors({
    origin: "http://localhost:3000",
    optionSuccessStatus: 200
}));
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/assets/', Express.static(__dirname + '/assets'));
app.use(session({
    key: 'user_id',
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        expires: 600000
    }
}));
app.use((req, res, next) => {
    if (req.cookies.user_id && !req.session.username) {
        res.clearCookie('user_id');
    }
    next();
});

app.use('/', Express.static(path.join(__dirname, '../../build')));
app.use("/api/", Route);
app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, '../../build/index.html'))
});
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../../build/index.html'))
});
app.use((req, res, next) => {
    next(httpError(404));
});

app.listen(PORT, () => {
    console.log("Server Listening to Port " + PORT);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.send(err.status);
})