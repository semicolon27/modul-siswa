import express from "express";
import {
    getSiswa,
    postSiswa,
    putSiswa,
    deleteSiswa,
    cariSiswa,
    getSiswaById
} from "../controllers/ControllerSiswa";
import { login, cekLogin, logout, loginInstameter, getProfile } from "../controllers/ControllerUsers";

const router = express.Router();

router.get("/siswa", getSiswa);
router.get("/siswa/:nis", getSiswaById);
router.get("/siswa/cari/:cari", cariSiswa);
router.post("/siswa", postSiswa);
router.put("/siswa/:nis", putSiswa);
router.delete("/siswa/:nis", deleteSiswa);

router.post("/login", login)
router.get("/islogin", cekLogin)
router.get("/logout", logout)

router.post("/instameter/login", loginInstameter)
router.get("/instameter/getprofile/:username", getProfile)


module.exports = router;