const Sequelize = require("sequelize");
const admin = require("../models/index").admin;
const instameter = require("../models/index").instameter;
const Op = Sequelize.Op;

exports.login = async(req, res) => {
    try {
        const login = await admin.findAll({
            where: {
                [Op.and]: [{
                        username: req.body.username
                    },
                    {
                        password: req.body.password
                    },
                ]
            }
        })
        if (login.length !== 1) {
            res.json({
                status: "notmatch",
                message: "Username dan/atau Password Salah"
            })
            res.redirect('/home');
            res.end()
        } else {
            req.session.loggedin = true;
            req.session.username = req.body.username;
            res.json({
                user: req.session.username,
                islogin: req.session.loggedin,
                status: 'loggedin',
                message: "Login Berhasil",
                data: req.session

            })
            res.end()
        }
    } catch (err) {
        res.json({
            status: 'err',
            message: `Terjadi Kesalahan Teknis, Mohon Coba Beberapa Saat Lagi atau Segere Melapor ke Teknisi \n${err.message}`
        })
    }
}

exports.loginInstameter = async(req, res) => {
    try {
        const login = await instameter.findAll({
            where: {
                [Op.and]: [{
                        username: req.body.username
                    },
                    {
                        password: req.body.password
                    },
                ]
            }
        })
        if (login.length !== 1) {
            res.json({
                status: "notmatch",
                message: "Username dan/atau Password Salah"
            })
            res.redirect('/home');
            res.end()
        } else {
            req.session.loggedin = true;
            req.session.username = req.body.username;
            res.json({
                user: req.session.username,
                islogin: req.session.loggedin,
                status: 'loggedin',
                message: "Login Berhasil",
                data: req.session

            })
            res.end()
        }
    } catch (err) {
        res.json({
            status: 'err',
            message: `Terjadi Kesalahan Teknis, Mohon Coba Beberapa Saat Lagi atau Segere Melapor ke Teknisi \n${err.message}`
        })
    }
}

exports.getProfile = async(req, res, next) => {
    try {
        const data = await instameter.findAll({
            where: {
                username: req.params.username
            }
        });
        if (data.length !== 0) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};


exports.cekLogin = async(req, res) => {
    try {
        if (req.session.loggedin) {
            res.json({
                islogin: req.session.loggedin,
                status: 'loggedin',
                message: "Anda Telah Login Sebagai " + req.session.username,
                user: req.session.username
            })
        } else {
            res.json({
                islogin: req.session.loggedin,
                status: '!loggedin',
                user: req.session.username,
                message: "Anda Belum Login",
                data: req.session
            })
        }
    } catch (err) {
        res.json({
            status: 'error',
            message: err.message
        })
    }
}

exports.logout = async(req, res) => {
    try {
        req.session.destroy()

    } catch (err) {
        console.log(err)
    }
}