const Sequelize = require("sequelize");
const siswa = require("../models/index").siswa;
const jk = require("../models/index").jenis_kelamin;
const Op = Sequelize.Op;

siswa.belongsTo(jk, { foreignKey: "id_jk" });

exports.getSiswa = async(req, res, next) => {
    try {
        const data = await siswa.findAll({
            include: [jk]
        });
        if (data.length !== 0) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};

exports.getSiswaById = async(req, res, next) => {
    try {
        const data = await siswa.findAll({
            include: [jk],
            where: {
                nis: req.params.nis
            }
        });
        if (data.length !== 0) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};

exports.cariSiswa = async(req, res, next) => {
    try {
        const param = req.params.cari;
        const search = {
            where: {
                [Op.or]: [{
                        nis: {
                            [Op.like]: `%${param}%`
                        }
                    },
                    {
                        nama: {
                            [Op.like]: `%${param}%`
                        }
                    },
                    {
                        id_jk: {
                            [Op.like]: `%${param === 1 ? "Laki - laki" : "Perempuan"}%`
                        }
                    },
                    {
                        kelas: {
                            [Op.like]: `%${param}%`
                        }
                    },
                    {
                        alamat: {
                            [Op.like]: `%${param}%`
                        }
                    }
                ]
            },
            include: [jk]
        };

        const data = await siswa.findAll(search);
        if (data.length !== 0) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};

exports.postSiswa = async(req, res, next) => {
    try {
        const { nis, nama, id_jk, kelas, alamat } = req.body;
        const data = await siswa.create({
            nis,
            nama,
            id_jk,
            kelas,
            alamat
        });
        if (data) {
            res.status(201).json({
                status: "OK",
                messages: "Siswa berhasil ditambahkan",
                data: data
            });
        }
    } catch (err) {
        res.json({
            data: req.body
        });
    }
};

exports.putSiswa = async(req, res, next) => {
    try {
        const id = req.params.nis;
        const { nis, nama, id_jk, kelas, alamat } = req.body;
        const result = await siswa.update({
            nis,
            nama,
            id_jk,
            kelas,
            alamat
        }, {
            where: {
                nis: id
            }
        });
        if (result) {
            res.json({
                status: "OK",
                messages: "Mobil berhasil diupdate",
                data: result
            });
        }
    } catch (err) {
        res.status(400).json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};
exports.deleteSiswa = async(req, res, next) => {
    try {
        const id = req.params.nis;
        const result = await siswa.destroy({
            where: {
                nis: id
            }
        });
        if (result) {
            res.json({
                status: "OK",
                messages: "Mobil berhasil dihapus",
                data: result
            });
        }
    } catch (err) {
        res.status(400).json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};