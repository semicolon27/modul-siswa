import React, { useState, onEffect } from 'react';
import Axios from 'axios';
import { Redirect } from "react-router-dom";

export default function Login(props) {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [status, setStatus] = useState("normal")
    const [message, setMessage] = useState("")
    const [user, setUser] = useState("")
    const handleLogin = async () => {
        let res = await Axios.post("http://localhost:7007/api/login", {username : username, password: password});
        let isLogin = await Axios.get("http://localhost:7007/api/islogin");
        console.log(isLogin.data)
        setStatus(res.data.status)
        setMessage(res.data.message)
        setUser(res.data.user)
        console.log(res.data)
    }
    return (
        <>
        <h1>Login Form</h1>
        		<input onChange={e => setUsername(e.target.value)} type="text" name="username" placeholder="Username" required />
				<input onChange={e => setPassword(e.target.value)} type="password" name="password" placeholder="Password" required />
                {/* {status === "loggedin" ? <Redirect to="/" /> : ""} */}
                <h1>{message}</h1>
                <h1>hai {user}</h1>
				<button onClick={() => handleLogin()}>Login</button>
        </>
    )
}