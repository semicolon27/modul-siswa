import React, { useState, useEffect } from "react";
import { Table } from "reactstrap";
import axios from 'axios';

export default function TableSiswa() {
  const [search, setSearch] = useState('');
    // const useFetch = (url, options) => {
        const [siswa, setSiswa] = React.useState([]);
        const [error, setError] = React.useState(null);
    //     React.useEffect(() => {
    //       const fetchData = async () => {
    //         try {
    //           const res = await fetch(url, options);
    //           const json = await res.json();
    //           console.log(json)
    //           setSiswa(json.data);
    //         } catch (error) {
    //           setError(error);
    //         }
    //       };
    //       fetchData();
    //     }, []);
    //     return { siswa, error };
    //   };
    //   let data = useFetch("http://localhost:7007/api/siswa", []);
    const getData = url => {
      axios.get(url).then(res => {
        setSiswa(res.data.data)
      });
    };
      
    useEffect(() => {
      getData('http://localhost:7007/api/siswa')
    }, [])
    console.log(siswa)
  let no = 0;
  return (
      <>
      <input type="text" onChange={e => setSearch(e.target.value)}/>
      {/* <button onSubmit={getData('http://localhost:7007/api/siswa/cari/'+search)} /> */}
    <Table striped>
      <thead>
        <tr>
          <th>#</th>
          <th>NIS</th>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Kelas</th>
          <th>Alamat</th>
        </tr>
      </thead>
      <tbody>
            {siswa.map(i => {
                no++
                return (
                    <tr>
                    <th scope="row">{no}</th>
                    <td>{i.nis}</td>
                    <td>{i.nama}</td>
                    <td>{i.jenis_kelamin.jk}</td>
                    <td>{i.kelas}</td>
                    <td>{i.alamat}</td>
                    </tr>
                )
            })}
      </tbody>
    </Table>
    </>
  );
}
