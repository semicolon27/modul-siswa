import React, { useState, useEffect } from "react";
import { Route, Redirect, Switch } from 'react-router-dom'
import TableSiswa from "./components/Table/TableSiswa";
import Login from "./components/LoginComponent/Login";
import axios from "axios";
function App() {
  const [loginStatus, setLoginStatus] = useState("")
  const [error, setError] = useState("");
  const [user, setUser] = useState("");
    useEffect(() => {
            const fetchData = async () => {
              try {
                let res = await axios.get("http://localhost:7007/api/islogin");
                let data = res.data;
                console.log(res)
                setLoginStatus(data.islogin);
                setError(data.status === 'error' ? data.status : '')
                setUser(data.user);
              } catch (err) {
                setError(err)
              }
            };
            fetchData();
          }, []);
          document.title = user
  return (
  <div className="App">
    <h1>Hai {user}</h1>
    {!loginStatus ? <Redirect to='/login' /> : ''}
    {console.log(loginStatus)}
    <Switch>
    <Route exact path="/login" component={Login} />
    <Route exact path="/siswa" component={TableSiswa} />
    </Switch>
  </div>
  );
}

export default App;
